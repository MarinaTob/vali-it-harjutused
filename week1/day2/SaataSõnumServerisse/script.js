console.log("Töötan!")

var refreshMessage = async function function_name() {
	// Lihtsalt selleks, et ma tean, et funktsioon läks käima
	console.log("Start läks käima")
	// Api adress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	// fetch teeb päringu serverisse (meie defineeritud adressile)
	var request = await fetch(APIurl)
	// json() käsk vormindab meile data mugavaks JSONiks	
	var json = await request.json()	
		// Kuva serverist saadud ifno HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages
	while (sonumid.length > 0) { // kuniks sõnumeid on
		var sonum = sonumid.shift() // võtab viimane sõnum from message

	//lisa HTMLi #jutt sisse sonum.message

	document.querySelector('#jutt').innerHTML += 
	"<p>" + sonum.user + ": " + sonum.message + "</p>"
	}
	//scrolli kõige alla
	window.scrollTo(0,document.body.scrollHeight);
}
// Uuenda sõnumeid iga sekund
setInterval(refreshMessage, 1000) // 1000 on sekund


document.querySelector('form').onsubmit = function(event){
	event.preventDefault() 
	//korjame kokku formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector("#message").value
	document.querySelector('#message').value=""//tee input tühjaks

	console.log(username, message)
	//POST päring postitab uue admetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message" // See on server poolt antud URL 
	
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({
			user: username, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})

	//console.log("submit käivitus")

	}