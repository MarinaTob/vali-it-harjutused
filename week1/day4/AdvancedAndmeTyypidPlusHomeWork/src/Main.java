
    import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

    public class Main {
        public static void main(String[] args) {
            //Keskseks teemaks on array ehk massiiv
            int[] massiiv = new int[6];
            ArrayList list = new ArrayList();

            //Ül: prindi välja massiiv
            String massiivStr = Arrays.toString(massiiv);
            System.out.println(massiivStr);

            //Ül: muuda kolmandal positsioonil olev number viieks
            massiiv[2] = 5;
            System.out.println(Arrays.toString(massiiv));

            //Ül: prindi välja viimane element massiivist (enne määra talle ka väärtus)
            massiiv[5] = 13;
            System.out.println(massiiv[5]);

            //Ül: prindi välja viimane element, ükskõik kui pikk massiiv ka ei oleks.
            int viimane = massiiv[massiiv.length - 1];
            System.out.println("viimane on " + viimane);

            //Ül: loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud
            int[] massiiv2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
            System.out.println(Arrays.toString(massiiv2));

            //Ül: prindi välja ükshaaval kõik väärtused massiiv2-st
            int index = 0;
            while (index < massiiv2.length) {
                System.out.println(massiiv2[index]);
                index++;
            }

            //Ül: teeme selle sama tsükli kiiremini kasutades for tsüklit
            for (int i = 0; i < massiiv2.length; i++) {
                System.out.println(massiiv2[i]);

            }
            //Ül: 1. Loo  Stringide massiiv, mis on alguses tühi
            //    2. aga siis lisad ka veel keskele mingi sõne.

            String[] stringid = new String[3];
            stringid[1] = "Kalevipoeg";
            System.out.println(Arrays.toString(stringid));

            String[] stringid1 = new String[9];
            stringid1[stringid.length / 2 - 1] = "Kalevipoeg";
            System.out.println(Arrays.toString(stringid1));

            // Ül: 1. loo massiiv, kus on 100 kohta.
            //     2. Täida massiiv järjest loeteluga alustades numbrist 0
            //     3. Prindi välja  mega massiiv

            int[] megaMass = new int[99];

            for (int i = 0; i < megaMass.length; i++) {
                System.out.println(i);
                megaMass[i] = i;
            }
            System.out.println(Arrays.toString(megaMass));

            // Ül:
            // 1. kasuta megaMass massiivi, kus on numbrite jada
            // 2. loe mitu paarisarvu on?
            // 3. prindi tulemus välja
            // kasutada tuleb nii tsükli, kui ka if lause
            // kui jagad kaks arvu operaatoriga % , siis see tagastab jäägi.
            // * % 2 tähendab, et 0 puhul on paarisarv ja 1 puhul ei ole.
            // Tegevus on järgmine:
            // 1. Võtan esimese numbri massiivist. See on 0.
            // 2. Ma küsin kas see number on paaris või mitte?
            // 3. Kuna on paaris, siis liidan ühe loendajale otsa.
            //
            // Alggoritm on järgmine:
            // 1. Võtan esimese numbri massiivist.
            //2. Kui number on paaris?
            //3. Siis lisan ühe loendajale otsa.


            int mituPaarisOn = 0;

            for (int i = 0; i < megaMass.length; i++) {

                //System.out.println(i); - chtoby posmotret' peremennuju v Debbugere !!!!


                if (i % 2 == 0) {

                    mituPaarisOn = mituPaarisOn + 1; // mituPaarisOn++;

                }
            }
            System.out.println("mituPaarisOn" + mituPaarisOn);

            // ÜL. Loo ArrayList ja sisesta sinna kolm numbrit ja kaks Stringi.

            ArrayList list2 = new ArrayList();
            list2.add(4);
            list2.add(5);
            list2.add(64687);
            list2.add("Krister");
            list2.add("Peeter");

            // Ül. Küsi viimasest List välja kolmas element ja prindi välja
            System.out.println(list2.get(2)); // - !!!

            //Ül. Prindi kogu list välja !!! okou, nii keeruline
            System.out.println(list2);

            //Ül. Prindi iga element ükshaaval väla

        /*
        int i=0;
        while (i<list2.size())
            System.out.println(list2.get(i));
            i++;
        }
        */
            // või:

            for (int i = 0; i < list2.size(); i++) {
                System.out.println(list2.get(i));
            }

            //Ül. Kotutöö:
            //1.   Loo uus ArrayList, kus on näiteks 543 numbrit.
            //1.1. Numbrid peavad olema suvalised. Nimelt Math.random() vahemikus 0-10.
            //2. Korruta iga number viiega.
            //3. Salvesta see uus number samale positsioonile.

        /*
           for (int i =0; i<100; i++) { // see on vaid vihje
           double nr = Math.random()*2;
           System.out.println(nr);
        }
        */
            // 1.:
            int[] elements = new int[543];
            // 1.1:
            for (int i = 0; i < elements.length; i++) {
                int a = 0;
                int b = 10;
                int random = a + (int) (Math.random() * b);
                elements[i] = random;
                //System.out.println(Arrays.toString(elements));
            }
            List<String> list3 = Arrays.asList(Arrays.toString(elements));
            System.out.println(list3);
            // 2., 3.:
            for (int i = 0; i < elements.length; i++) {
                elements[i] = elements[i] * 5;
            }
            List<String> list4 = Arrays.asList(Arrays.toString(elements));
            System.out.println(list4);

            // ---------------------------------------------//

           ArrayList list543 = new ArrayList();
           for (int i = 0; i<543; i++) {
               double nr = Math.floor(Math.random()*11);
               //System.out.println(nr);
               list543.add(nr);
            }
            System.out.println("Algne massiiv: " + list543);

            for (int i = 0; i<list543.size(); i++) {
               double muudetudNr = (double) list543.get(i)*5;  //    element korrutautd viiega  NB!!!! syntaks (int) !!!
               list543.set(i,muudetudNr);
            }
            System.out.println("Muudetud massiv: " + list543);

           //-----------------------------


            ArrayList <Integer> list543uus = new ArrayList();

            for (int i = 0; i<543; i++) {
                int nr = (int) Math.floor(Math.random()*11);
                //System.out.println(nr);
                list543uus.add(nr);
            }
            System.out.println("Algne massiiv: " + list543);

            for (int i = 0; i<list543.size(); i++) {
                int muudetudNr = (int) list543uus.get(i)*5;  //    element korrutautd viiega  NB!!!! syntaks (int) !!!
                list543uus.set(i,muudetudNr);
            }
            System.out.println("Muudetud massiv: " + list543uus);



        }
    }








































