import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        System.out.println("Hi!");

        //Ül:
        //1. Loo kolm muutujat nubritega
        //2. Moodusta lause nende muutujatega
        //3. Prindi see lause välja
        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = "Mina sündisin aastal " + aasta + ". Kuu oli " +kuu+ ". ja kuupäev " +paev+".";
        System.out.println(lause);

        //%d on täisarv
        //%f on komakohaga
        //%s on string
        String parem = String.format("Mina sündisin aastal %d. Kuu oli %d. ja kuupäev %d.", aasta, kuu, paev);
        System.out.println(parem);

        //--------------------------------- HashMap -------------------------------------------//

        // Ül1: loe kokku mitu lampi on klassis ja pane see info HashMappi!
        // ÜL2: loe kokku mitu akent on klassis ja pane see ifno HashMappi!
        // Ül3: Loe kokku mitu inimest on klassis ja pane see ifno HashMappi!

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lambid", 11);
        klassiAsjad.put("inimesed", 21);
        //klassiAsjad.put("lambid", 90);     -kirjutab üle

        System.out.println(klassiAsjad);

        // Ül.: Mõtle välja 3 praktilist kasutust kasutust HashMapile, kus sellist struktuuri tuleks kasutada.
        // 1. haiglas on (patsiendid ID: nimi);
        // 2. Trammis on (roheliseKaardiId: kontoJääk);
        // 3. Laual on (L: 5mm, M: 3mm, F: 1mm) markerid

       // Ül.: Prindi välja kui palju on inimesi klassis, kasutades juba loodud hashmappi.

       int inimesi = (int) klassiAsjad.get("inimesed");
       System.out.println(inimesi);

       // Ül.: Lisa samassa HashMappi juurde mitu tasapinda on klassis, aga number enne ja siis String
       // näiteks (10: "tasapinnad")

       klassiAsjad.put(10, "tasapinnad");
       System.out.println(klassiAsjad);

       //int tasapinnad = (int) klassiAsjad.get("tasapinnad");                            -- peredelat' !!!!!
       //System.out.println(tasapinnad);

       // Ül.: loo uus HashMap, kuhu saav sisestada AINULT String:double paare. Sisesta sinna ka midagi.

       HashMap <String, Double> tyybidHashMapp = new HashMap<>();
       tyybidHashMapp.put("Number", 2.0);
       System.out.println(tyybidHashMapp);

       // Ül.: switch

        int rongiNr = 50;
        String suund = null;

        switch (rongiNr) {                          // perebirajet vsjo po porjadku, esli ne napisano break !!!!!!!
            case 50: suund = "Pärnu";
            case 56: suund = "Rakvere";
            case 55: suund = "Haapsalu"; break;
            case 10: suund = "Vormsi";
        }
        System.out.println(suund);

        // FOREACH

        int[] mingiNumbrid = new int[]{8, 4 ,6 ,8978 ,1234567};

        for (int i=0; i<mingiNumbrid.length; i++) {
            System.out.println(mingiNumbrid[i]);
        }

        System.out.println("------------------------------------");

        for (int nr : mingiNumbrid) {
            System.out.println(nr);
        }

        // Ül.: Õpilane saab töös punkte 0-100
        // 1. kui punkte on alla 50, kukub ta töö läbi.
        // 2. vastasel juhul on hinne täisarvuline punktid/20
        // 100 punkti -> 5
        //  80 punkti -> 4
        //  75 punkti -> 3
        //  50 punkti -> 2


        int punkte = 99;

        if (punkte > 100 || punkte < 0){
            throw new Error();
        }

        switch (punkte/20) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee.. õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi");



        }




    }
}
