import nadalapaevad.Laupaev;
import nadalapaevad.Puhapaev;
import nadalapaevad.Reede;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        if ((false && false) || true) {  // boolean
            System.out.println("Tõene");
        }else{
            System.out.println("Väär");
        }

        Reede.koju();
        // reede on klass, koju on meetod.
        // klassi ja meetodi loomise shortcut: klikka peale ja Alt+Enter

        Laupaev.peale();

        Puhapaev.hommik();

        Puhapaev paev = new Puhapaev();
        paev.maga();
        //paev.uni();  // see on private, sp punane alla


        paev.hommik();

    }
}







