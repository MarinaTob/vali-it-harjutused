import java.util.ArrayList;
import java.util.List;

public class Võistlus {
    ArrayList<Suusataja>voistlejad;
    private static List<Suusataja> traumeeritudList = new ArrayList<>();

    int koguDistans;

    public Võistlus() {
        System.out.println("Start");
        voistlejad = new ArrayList();
        koguDistans = 20;
        for (int i = 0; i < 10; i++) {
            voistlejad.add(new Suusataja(i));
        }
        voistlejad.get(1+ (int) (Math.random() * 9)).kiirus*= 7; // "lucky 7" boosts speed 2 times
        aeg();
    }
    private void aeg() {
        for (Suusataja s:voistlejad) {
            s.suusata();
            if (1+ (int)(Math.random()*5000)==13){ // Unlucky number 13
                traumeeritudList.add(s);
                voistlejad.remove(s);
                break;
            }
            System.out.println(s);
            boolean lopetanud = s.kasOnLopetanud(koguDistans);
            if (lopetanud) {
                System.out.println("Võitja on: " + s);

                System.out.println("\nTraumeeritud :");
                for (Suusataja sk : traumeeritudList){
                    System.out.println(sk);
                }
                return;
            }
        }
        //System.out.println(voistlejad);
        System.out.println("-------------------");

        try {
            Thread.sleep(1);  // 1000 = magada 1-ks sekundiks
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aeg();
    }
}
