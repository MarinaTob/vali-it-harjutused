import java.util.List;

public class Application {
    public static void main(String[] args) {

        System.out.println("Võistlus alga!");
        List<Suusataja> voistlejad = Suusataja.createVoislejadList();
        System.out.println("Start");
        Võistlus.aeg(voistlejad);
        System.out.println("\n\nWinner : " + Võistlus.winner.stardiNr + " !!!");
        System.out.println("\nTraumeeritud : ");
        for (Suusataja s : Võistlus.traumeeritud) System.out.println("Suusataja " + s.stardiNr);

    }


}
