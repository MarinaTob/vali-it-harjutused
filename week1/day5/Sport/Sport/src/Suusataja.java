import java.util.ArrayList;
import java.util.List;

public class Suusataja {
    int stardiNr;
    private double kiirus;
    private double labitudDist;

    private Suusataja(int i) {
        double dopinguKordaja = dopingControl();
        this.stardiNr = i;
        this.kiirus = Math.random() * 20 * dopinguKordaja; // 20 km/t
        this.labitudDist = 0;
    }

    private double dopingControl() {
        if (1 + (int) (Math.random() * 10) == 7) { // "lucky 7" boosts speed 2 times
            return 2;
        } else {
            return 1;
        }
    }

    static List<Suusataja> createVoislejadList() {
        List<Suusataja> listWithSuusatajad = new ArrayList<>();
        for (int j = 0; j < 10; j++) {
            listWithSuusatajad.add(new Suusataja(j));
        }
        return listWithSuusatajad;
    }

    void suusata() {
        labitudDist += kiirus / (3600);
    }

    public String toString() {
        int dist = (int) (labitudDist);
        StringBuilder nool = new StringBuilder(stardiNr + ": ");
        for (int i = 0; i < dist; i++) {
            nool.append("=");
        }
        nool.append("=");
        return nool.toString();
    }

    boolean kasOnLopetanud(int koguDistants) {
        return labitudDist >= koguDistants;
    }
}
