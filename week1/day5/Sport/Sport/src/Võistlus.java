import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("NonAsciiCharacters")
class Võistlus {
    static Suusataja winner;
    static List<Suusataja> traumeeritud = new ArrayList<>();

    static void aeg(List<Suusataja> voistlejad) {
        for (Suusataja s : voistlejad) {
            s.suusata();
            if (1 + (int) (Math.random() * 10000) == 13) { //Unlucky number 13
                traumeeritud.add(s);
                voistlejad.remove(s);
                break;
            }
            System.out.println(s);
            int koguDistans = 20;
            boolean lopetanud = s.kasOnLopetanud(koguDistans);
            if (lopetanud) {
                winner = s;
                return;
            }
        }

        System.out.println("-------------------");
        try {
            Thread.sleep(1);  // 1000 = magada 1-ks sekundiks
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aeg(voistlejad);
    }


}