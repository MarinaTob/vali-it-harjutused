package ee.valiit.chat;

public class ChatMessage {
    private int id;
    private String username;
    private String room;
    private String message;
    private String pilt;

    public ChatMessage() { }

    public ChatMessage(String username, String message, String pilt) {
        this.username = username;
        this.message = message;
        this.pilt = pilt;


    }



    public int getId() {
        return id;
    }

    public String getUsername() {
        return Security.xssFix(username);
    }

    public String getRoom() {
        return room;
    }

    public String getPilt ( ) {
        return Security.xssFix(pilt); }

    public String getMessage() {
        return Security.xssFix(message);
    }
}
