package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.Configuration;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
@CrossOrigin
public class APIController {

    @Autowired                     //annotation - käsk: ühenda klass; JDBC depandancy
            JdbcTemplate jdbsTemplate;

    @GetMapping("/chat/{room}")
    ArrayList<ChatMessage> chat(@PathVariable String room) {

        try {

            String sqlKask = "SELECT * FROM messages WHERE room = '" + room + "'";
            ArrayList<ChatMessage> messages = (ArrayList) jdbsTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String username = resultSet.getString("username");
                String message = resultSet.getString("message");
                String pilt = resultSet.getString("pilt");
                return new ChatMessage(username, message, pilt);
            });

            return messages;

        } catch (DataAccessException err) {      // err - on oma nimi
            System.out.println("TABLE WAS NOT READY");
            return new ArrayList();
        }
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {

        String sqlKask = "INSERT INTO messages (username, message, pilt, room) VALUES ('" +
                            msg.getUsername() + "', '" +
                            msg.getMessage() + "', '" +
                            msg.getPilt() + "', '" +
                            room + "')";
        jdbsTemplate.execute(sqlKask);


    }

}