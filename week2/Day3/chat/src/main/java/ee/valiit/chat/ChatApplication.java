package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class ChatApplication implements CommandLineRunner {

	/* Ülesanded:
	1. Profiili pilt uuesti teha
	2. Jututoa vahetamine jälle tööle

	*/

	public static void main(String[] args) {
		SpringApplication.run(ChatApplication.class, args);
	}

	@Autowired
	JdbcTemplate jdbsTemplate;

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Configure database tables");
		jdbsTemplate.execute("DROP TABLE IF EXISTS messages");
		jdbsTemplate.execute("CREATE TABLE messages (id SERIAL, username TEXT, message TEXT, pilt TEXT, room TEXT)");

	}
}
