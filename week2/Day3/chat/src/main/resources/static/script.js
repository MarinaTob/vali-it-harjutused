console.log("Töötan!")

var refreshMessage = async function function_name() {
	// Lihtsalt selleks, et ma tean, et funktsioon läks käima
	console.log("Start läks käima")
	// Uus muutuja, kuhu salvestame praeguse toa
	var tuba= document.querySelector("#room").value
	// Api adress on string, salvestan lihtsalt muutujasse
	//var APIurl = "http://localhost:8080/chat/general"
	var APIurl = "http://localhost:8080/chat/" + tuba                         // !!!!!!!!!

	// fetch teeb päringu serverisse (meie defineeritud aadressile)
	var request = await fetch(APIurl)
	// json() käsk vormindab meile data mugavaks JSONiks	
	//var json = await request.json()
	var sonumid = await request.json();
	console.log(sonumid);

	// Kuva serverist saadud ifno HTMLis (ehk lehel)
	document.querySelector('#jutt').innerHTML = ""
	//var sonumid = json.messages
	while (sonumid.length > 0) { // kuniks sõnumeid on
		var sonum = sonumid.shift() // võtab viimane sõnum from message

	//lisa HTMLi #jutt sisse sonum.message

	document.querySelector('#jutt').innerHTML +=

	// "<p> <img src='"+sonum.avatar+"'>"+ sonum.user + ": " + sonum.message + "</p>"

	//"<p>" <img src='"+sonum.pilt+"'> + sonum.username + ": " + sonum.message + "</p>"

	`<p><img src='${sonum.pilt}' width="100">${sonum.username}: ${sonum.message}</p>` // zeljonym prosto potomu chto IntelliJ tak reshil
	}

	//scrolli kõige alla
	window.scrollTo(0,document.body.scrollHeight);
}
// Uuenda sõnumeid iga sekund
setInterval(refreshMessage, 1000) // 1000 on sekund


document.querySelector('form').onsubmit = function(event){
	event.preventDefault() 
	//korjame kokku formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector("#message").value
	var pilt = document.querySelector("#pilt").value
	document.querySelector('#message').value=""//tee input tühjaks

	console.log(username, message)
	//POST päring postitab uue admetüki serverisse
    //Stringi sisse muutuja läheb: "+ +"

    var tuba = document.querySelector("#room").value
    // API aadress on string, salvestan lihtsalt muutujasse
    var APIurl = "http://localhost:8080/chat/" + tuba + "/new-message" // See on server poolt antud URL
	//var APIurl = "http://localhost:8080/chat/general/new-message" // See on server poolt antud URL
	
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({username: username, message: message, pilt: pilt}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})

	//console.log("submit käivitus")

	}