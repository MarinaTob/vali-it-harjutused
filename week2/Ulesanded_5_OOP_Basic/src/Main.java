import java.util.ArrayList;

public abstract class Main {

    public static void main(String[] args) {

        ArrayList<Athlete>sportlased = new ArrayList<>();
            sportlased.add(new Skydiver("Kukk"));
            sportlased.add(new Skydiver("Kana"));
            sportlased.add(new Skydiver("Hani"));
            sportlased.add(new Runner("Tiiger"));
            sportlased.add(new Runner("Puuma"));
            sportlased.add(new Runner("Gepard"));

        /*
        Skydiver langevarjur1 = new Skydiver("Kukk");                 // -Objektide initsialiseerimine
        Skydiver langevarjur2 = new Skydiver("Kana");
        Skydiver langevarjur3 = new Skydiver("Hani");
        Runner jooksja1 = new Runner("Tiiger");
        Runner jooksja2 = new Runner("Puuma");
        Runner jooksja3 = new Runner("Gepard");
        */

        //langevarjur1.getClass().getDeclaredFields();

        for (Athlete sportlane:sportlased) {
            System.out.println(sportlane.perenimi);
            System.out.println(sportlane.eesnimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal);

            sportlane.perform();

        }

    }


}
