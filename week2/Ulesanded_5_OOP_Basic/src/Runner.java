public class Runner extends Athlete {


    public Runner(String perenimi) {
        super(perenimi);
    }

    @Override
    public void perform() {
        System.out.println(perenimi + ": Pikk samm, pikk samm, pikk samm");

    }
}
