import java.math.BigInteger;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        System.out.println(Test.test(1));
        boolean n=Test.test(5);
        System.out.println(n);

        Test.test2("jla","hag");

        Test.addVat(5.2);
        System.out.println(Test.addVat(5.2));
        double b = 5.2;
        System.out.println(Test.addVat(b));


        String massiivStr = Arrays.toString(Test.massiiv(5,3,true));
        System.out.println(massiivStr);

        String isikukood = "37806170281";
        String gender = Test.deriveGender(isikukood);
        System.out.println(gender);
        System.out.println(Test.deriveGender(isikukood));

        Test.printHello();

        Test.deriveBirthYear("50602460212");


        BigInteger ik = new BigInteger("47806170281");
        Test.validatePersonalCode(ik);
        System.out.println(Test.validatePersonalCode(ik));


    }
}
