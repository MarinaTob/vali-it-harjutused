package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {
    private String user;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;
    private String pilt;

    public ChatMessage() {
    }

    public ChatMessage(String user, String message, String pilt) {
        this.user = user;
        this.message = message;
        this.date = new Date();
        this.pilt = pilt;
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getPilt() {
        return pilt;
    }
}
