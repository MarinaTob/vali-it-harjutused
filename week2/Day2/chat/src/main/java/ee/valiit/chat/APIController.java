package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/* Iseseisev ülesanne:
    1. (lihtsam) peale useri ja message lissa ka profiili pildi URl, mis salvestub serverisse ja ka kuvatakse HTML välja.
    Nimelt, et kasutaja saab kolmada välja kuhu see URl panna.
    2. (keerulisem) Loo uus chatroom peale "general".
*/
@RestController
@CrossOrigin
public class APIController {


    //ChatRoom general = new ChatRoom("general");

    HashMap<String, ChatRoom> rooms = new HashMap();

    public APIController () {
        rooms.put("general", new ChatRoom("general"));
        rooms.put("random", new ChatRoom("random"));
        rooms.put("materjalid", new ChatRoom("materjalid"));
    }




    @GetMapping("/chat/{room}")
    ChatRoom chat(@PathVariable String room) {
        return rooms.get(room);
    }
    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        rooms.get(room).addMessage(msg);
    }


    /*
    ChatRoom random = new ChatRoom("random");

    @GetMapping("/chat/random")
    ChatRoom jutt() {return random; }
    @PostMapping("/chat/random/new-message")
    void uusMessage(@RequestBody ChatMessage msg) {random.addMessage(msg); }
    */
}